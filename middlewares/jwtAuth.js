const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
    try {
        console.log("MIDDL");
        const token = req.headers.authorization.split(" ")[1];
        console.log("MIDDL", token);

        const user = jwt.verify(token, process.env.JWT_SECRET, { secretalgorithms: ["HS256"] });
        console.log("MIDDL VER");

        req.user = user;
        return next();
    } catch (e) {
        if (e instanceof jwt.TokenExpiredError) {
            return res.status(401).json(e.message);
        } if (e instanceof jwt.JsonWebTokenError) {
            return res.status(403).json(e.message);
        }
        return res.status(500).json("error in token");
    }
};
