const userRoles = require("../util/user.roles");
const { returnErrorResponse } = require("../util/response");

module.exports = (req, res, next) => {
    if (req.user.role === userRoles.writer) return next();
    return returnErrorResponse(403, { message: "You have no permissions to do that!" }, res);
};
