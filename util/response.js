module.exports.returnSuccessResponse = (statusCode, dataObject, res) => res
    .status(statusCode)
    .json(dataObject);

module.exports.returnErrorResponse = (statusCode, error, res) => res
    .status(statusCode)
    .json({ message: error.message, error });
