const nodemailer = require("nodemailer");

module.exports = class Mailer {
    static getTransporter() {
        return nodemailer.createTransport({
            // host: "smtp.ethereal.email",
            service: "gmail",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
                user: process.env.EMAIL_ADDRESS, // "qassed.mail@gmail.com"
                // generated ethereal user
                pass: process.env.EMAIL_PASSWORD, // generated ethereal password
            },
        });
    }

    static async sentVerificationMail(email, code) {
        console.log("Mailer");
        const transporter = Mailer.getTransporter();
        const info = await transporter.sendMail({
            from: "Control Panel: Verification Code", // sender address
            to: email, // list of receivers
            subject: "Verification Code", // Subject line
            text: "Your verification code is: ", // plain text body
            html: `<h3>Your verification code is: - رمز التحقق الخاص بك هو:</h3><b>${code}</b>`, // html body
        });
        return info;
    }
};
