module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable("Sections", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.BIGINT,
            },
            articleId: {
                type: Sequelize.BIGINT,
                allowNull: false,
                references: {
                    model: {
                        tableName: "Articles",
                    },
                    key: "id",
                },
            },
            image: {
                type: Sequelize.STRING,
            },
            title: {
                type: Sequelize.STRING,
            },
            text: {
                type: Sequelize.TEXT,
            },
            order: {
                type: Sequelize.INTEGER,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        });
    },
    // eslint-disable-next-line no-unused-vars
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable("Sections");
    },
};
