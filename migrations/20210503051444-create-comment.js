module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable("Comments", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.BIGINT,
            },
            userId: {
                type: Sequelize.BIGINT,
                allowNull: false,
                references: {
                    model: {
                        tableName: "Users",
                    },
                    key: "id",
                },
            },
            articleId: {
                type: Sequelize.BIGINT,
                allowNull: false,
                references: {
                    model: {
                        tableName: "Articles",
                    },
                    key: "id",
                },
            },
            baseComment: {
                type: Sequelize.BIGINT,
                allowNull: true,
                references: {
                    model: {
                        tableName: "Comments",
                    },
                    key: "id",
                },
            },
            text: {
                type: Sequelize.STRING,
            },
            image: {
                type: Sequelize.STRING,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        });

        await queryInterface.sequelize.query(`CREATE TRIGGER inc_comments_count AFTER INSERT ON comments For EACH ROW
        BEGIN
            UPDATE articles SET commentsCount = commentsCount + 1 WHERE id = new.articleId;
        END;`);

        await queryInterface.sequelize.query(`CREATE TRIGGER dec_comments_count AFTER DELETE ON comments For EACH ROW
        BEGIN
            UPDATE articles SET commentsCount = commentsCount - 1 WHERE id = old.articleId;
        END;`);
    },
    // eslint-disable-next-line no-unused-vars
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable("Comments");
    },
};
