module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable("Articles", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.BIGINT,
            },
            title: {
                type: Sequelize.STRING,
            },
            keywords: {
                type: Sequelize.TEXT,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            userId: {
                type: Sequelize.BIGINT,
                allowNull: false,
                references: {
                    model: {
                        tableName: "Users",
                    },
                    key: "id",
                },
            },
            categoryId: {
                type: Sequelize.BIGINT,
                allowNull: false,
                references: {
                    model: {
                        tableName: "Categories",
                    },
                    key: "id",
                },
            },
            readsCount: {
                type: Sequelize.BIGINT,
                allowNull: false,
                defaultValue: 0,
            },
            commentsCount: {
                type: Sequelize.BIGINT,
                allowNull: false,
                defaultValue: 0,
            },
            likesCount: {
                type: Sequelize.BIGINT,
                allowNull: false,
                defaultValue: 0,
            },
        });

        await queryInterface.sequelize.query(`CREATE TRIGGER inc_articles_count AFTER INSERT ON articles For EACH ROW
        BEGIN
            UPDATE writers SET articlesCount = articlesCount + 1 WHERE userId = new.userId;
        END;`);

        await queryInterface.sequelize.query(`CREATE TRIGGER dec_articles_count AFTER DELETE ON articles For EACH ROW
        BEGIN
            UPDATE writers SET articlesCount = articlesCount - 1 WHERE userId = old.userId;
        END;`);
    },
    // eslint-disable-next-line no-unused-vars
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable("Articles");
    },
};
