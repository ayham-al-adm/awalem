module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable("Users", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.BIGINT,
            },
            name: {
                type: Sequelize.STRING,
            },
            image: {
                type: Sequelize.STRING,
            },
            email: {
                type: Sequelize.STRING,
            },
            password: {
                type: Sequelize.STRING,
            },
            role: {
                type: Sequelize.STRING,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            readsCount: {
                type: Sequelize.BIGINT,
                allowNull: false,
                defaultValue: 0,
            },
            bio: {
                type: Sequelize.STRING,
            },
            likesCount: {
                type: Sequelize.BIGINT,
                allowNull: false,
                defaultValue: 0,
            },
            verificationCode: {
                type: Sequelize.STRING,
            },
            verified: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
        });
    },
    // eslint-disable-next-line no-unused-vars
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable("Users");
    },
};
