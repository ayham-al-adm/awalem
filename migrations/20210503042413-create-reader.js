module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable("Readers", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.BIGINT,
            },
            facebookId: {
                type: Sequelize.STRING,
            },
            facebookToken: {
                type: Sequelize.STRING,
            },
            googleId: {
                type: Sequelize.STRING,
            },
            googleToken: {
                type: Sequelize.STRING,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            userId: {
                type: Sequelize.BIGINT,
                references: {
                    model: {
                        tableName: "Users",
                    },
                    key: "id",
                },
                allowNull: false,
            },
        });
    },
    // eslint-disable-next-line no-unused-vars
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable("Readers");
    },
};
