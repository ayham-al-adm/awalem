module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable("Saves", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.BIGINT,
            },
            userId: {
                type: Sequelize.BIGINT,
                allowNull: false,
                references: {
                    key: "id",
                    model: {
                        tableName: "users",
                    },
                },
            },
            articleId: {
                type: Sequelize.BIGINT,
                allowNull: false,
                references: {
                    key: "id",
                    model: {
                        tableName: "articles",
                    },
                },
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        });

        await queryInterface.addIndex("Saves", ["userId", "articleId"], {
            unique: true,
        });
    },
    // eslint-disable-next-line no-unused-vars
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable("Saves");
    },
};
