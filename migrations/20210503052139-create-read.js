module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable("Reads", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.BIGINT,
            },
            userId: {
                type: Sequelize.BIGINT,
                allowNull: false,
                references: {
                    model: {
                        tableName: "Users",
                    },
                    key: "id",
                },
            },
            articleId: {
                type: Sequelize.BIGINT,
                allowNull: false,
                references: {
                    model: {
                        tableName: "Articles",
                    },
                    key: "id",
                },
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        });

        await queryInterface.addIndex("Reads", ["userId", "articleId"], {
            unique: true,
        });

        await queryInterface.sequelize.query(`CREATE TRIGGER inc_reads_count AFTER INSERT ON \`reads\` For EACH ROW
        BEGIN
        DECLARE writerId BIGINT;
            SELECT userId INTO writerId FROM articles WHERE id = new.articleId;
            UPDATE writers SET readsCount = readsCount + 1 WHERE userId = writerId;
            UPDATE users SET readsCount = readsCount + 1 WHERE id = new.userId;
            UPDATE articles SET readsCount = readsCount + 1 WHERE id = new.articleId;
        END;`);

        await queryInterface.sequelize.query(`CREATE TRIGGER dec_reads_count AFTER DELETE ON \`reads\` For EACH ROW
        BEGIN
        DECLARE writerId BIGINT;
            SELECT userId INTO writerId FROM articles WHERE id = old.articleId;
            UPDATE writers SET readsCount = readsCount - 1 WHERE userId = writerId;
            UPDATE users SET readsCount = readsCount - 1 WHERE id = old.userId;
            UPDATE articles SET readsCount = readsCount - 1 WHERE id = old.articleId;
        END;`);
    },
    // eslint-disable-next-line no-unused-vars
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable("Reads");
    },
};
