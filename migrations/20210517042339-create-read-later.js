module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable("ReadLaters", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            userId: {
                type: Sequelize.BIGINT,
                allowNull: false,
                references: {
                    key: "id",
                    model: {
                        tableName: "users",
                    },
                },
            },
            articleId: {
                type: Sequelize.BIGINT,
                allowNull: false,
                references: {
                    key: "id",
                    model: {
                        tableName: "articles",
                    },
                },
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        });

        await queryInterface.addIndex("ReadLaters", ["userId", "articleId"], {
            unique: true,
        });
    },
    // eslint-disable-next-line no-unused-vars
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable("ReadLaters");
    },
};
