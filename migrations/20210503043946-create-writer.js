module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable("Writers", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.BIGINT,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            readsCount: {
                type: Sequelize.BIGINT,
                allowNull: false,
                defaultValue: 0,
            },
            likesCount: {
                type: Sequelize.BIGINT,
                allowNull: false,
                defaultValue: 0,
            },
            articlesCount: {
                type: Sequelize.BIGINT,
                allowNull: false,
                defaultValue: 0,
            },
            userId: {
                type: Sequelize.BIGINT,
                references: {
                    model: {
                        tableName: "Users",
                    },
                    key: "id",
                },
                allowNull: false,
            },
        });
    },
    // eslint-disable-next-line no-unused-vars
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable("Writers");
    },
};
