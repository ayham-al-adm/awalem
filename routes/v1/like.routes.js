const router = require("express").Router();
const likeController = require("../../controllers/v1/like");
const jwtAuth = require("../../middlewares/jwtAuth");

router.post("/", jwtAuth, likeController.addLike);
router.delete("/", jwtAuth, likeController.deleteLike);

module.exports = router;
