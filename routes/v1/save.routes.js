const router = require("express").Router();
const jwtAuth = require("../../middlewares/jwtAuth");
const saveController = require("../../controllers/v1/save");

router.get("/by-category", jwtAuth, saveController.getSavedArticlesByCategory);
router.get("/", jwtAuth, saveController.getSaves);
router.post("/", jwtAuth, saveController.addSave);
router.delete("/", jwtAuth, saveController.deleteSave);

module.exports = router;
