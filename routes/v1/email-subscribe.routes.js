const router = require("express").Router();
const subscribes = require("../../controllers/v1/email-subscribe");

router.post("/", subscribes.add);

module.exports = router;
