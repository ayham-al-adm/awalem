const router = require("express").Router();
const readController = require("../../controllers/v1/read");
const jwtAuth = require("../../middlewares/jwtAuth");

router.get("/recently-by-category", jwtAuth, readController.getRecentlyReadArticlesByCategory);
router.get("/", jwtAuth, readController.getReads);
router.post("/", jwtAuth, readController.addRead);

module.exports = router;
