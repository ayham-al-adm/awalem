const router = require("express").Router();
const multer = require("multer");
const uploadController = require("../../controllers/v1/uploads");

const storage = multer.diskStorage({ // multers disk storage settings
    destination(req, file, cb) {
        cb(null, "./public/uploads/tmp/");
    },
    filename(req, file, cb) {
        // console.log(file);
        const datetimestamp = Date.now();
        cb(null, `${file.fieldname}-${datetimestamp}.${file.originalname.split(".")[file.originalname.split(".").length - 1]}`);
    },
});

// const uploadMultiple = multer({ // multer settings
//     storage,
// }).array("files", 20);

const uploadSingle = multer({ // multer settings
    storage,
}).single("file");

router.post("/single-image", uploadSingle, uploadController.uploadImage);

module.exports = router;
