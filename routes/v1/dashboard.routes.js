const router = require("express").Router();
const dashboard = require("../../controllers/v1/dashboard");

router.put("/users/:id", dashboard.editUser);
router.delete("/users/:id", dashboard.deleteUser);
router.get("/users", dashboard.getUsers);
router.post("/users", dashboard.addUser);

router.put("/articles/:id", dashboard.editArticle);
router.delete("/articles/:id", dashboard.deleteArticle);
router.get("/articles", dashboard.getArticles);
router.post("/articles", dashboard.addArticle);

router.put("/sections/:id", dashboard.editSection);
router.delete("/sections/:id", dashboard.deleteSection);
router.get("/sections", dashboard.getSections);
router.post("/sections", dashboard.addSection);

router.put("/comments/:id", dashboard.editComment);
router.delete("/comments/:id", dashboard.deleteComment);
router.get("/comments", dashboard.getComments);
router.post("/comments", dashboard.addComment);

router.put("/categories/:id", dashboard.editCategory);
router.delete("/categories/:id", dashboard.deleteCategory);
router.get("/categories", dashboard.getCategories);
router.post("/categories", dashboard.addCategory);

module.exports = router;
