const router = require("express").Router();
const subscribeController = require("../../controllers/v1/subscribe");
const jwtAuth = require("../../middlewares/jwtAuth");

router.post("/", jwtAuth, subscribeController.addSubscribe);
router.delete("/", jwtAuth, subscribeController.deleteSubscribe);

module.exports = router;
