const router = require("express").Router();
const jwtAuth = require("../../middlewares/jwtAuth");
const readLaterController = require("../../controllers/v1/read-later");

router.get("/by-category", jwtAuth, readLaterController.getReadlaterArticlesByCategory);
router.get("/", jwtAuth, readLaterController.getReadLaters);
router.post("/", jwtAuth, readLaterController.addReadLater);
router.delete("/", jwtAuth, readLaterController.deleteReadLater);

module.exports = router;
