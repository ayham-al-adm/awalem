const router = require("express").Router();
const categoryController = require("../../controllers/v1/category");
const jwtAuth = require("../../middlewares/jwtAuth");
const isAdmin = require("../../middlewares/isAdmin");

router.get("/:categoryId/articles", categoryController.getArticles);
router.post("/", jwtAuth, isAdmin, categoryController.addCategory);
router.get("/:categoryId", categoryController.getCategoryData);
router.get("/", categoryController.getAllWithSubs);

module.exports = router;
