const router = require("express").Router();
const jwtAuth = require("../../middlewares/jwtAuth");
const commentRoutes = require("../../controllers/v1/comment");

router.get("/:id/replies", jwtAuth, commentRoutes.getReplies);
router.post("/", jwtAuth, commentRoutes.addComment);
router.delete("/", jwtAuth, commentRoutes.deleteComment);
module.exports = router;
