const router = require("express").Router();
const articlesController = require("../../controllers/v1/articles");
const jwtAuth = require("../../middlewares/jwtAuth");
const isWriter = require("../../middlewares/isWriter");

router.get("/top", articlesController.getTopArticles);
router.get("/top-by-category", articlesController.getTopArticlesByCategory);
router.get("/search", articlesController.search);
router.get("/search-relative", articlesController.searchRelative);
router.get("/new-by-category", articlesController.getNewArticlesByCategory);
router.get("/:id/comments", articlesController.getComments);
router.get("/:id/i-like", jwtAuth, articlesController.iLike);
router.get("/:id/i-save", jwtAuth, articlesController.iSave);
router.get("/:id", articlesController.getArticle);
router.post("/", jwtAuth, isWriter, articlesController.addArticle);
module.exports = router;
