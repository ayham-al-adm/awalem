const router = require("express").Router();
const authController = require("../../controllers/v1/auth");

router.post("/signup", authController.signup);
router.post("/writer-signup", authController.signupWriter);
router.post("/verify-email", authController.verifyEmail);
router.post("/login", authController.login);
router.post("/refresh-token", authController.refreshToken);
router.post("/facebook-login", authController.loginByFacebook);
router.post("/google-login", authController.loginByGoogle);
router.post("/resend-verification-code", authController.resendVerificationCode);
router.post("/reset-password", authController.resetPassword);

module.exports = router;
