const router = require("express").Router();
const userController = require("../../controllers/v1/user");

router.get("/rates", userController.getUserRate);
router.get("/best-readers", userController.getBestReaders);
router.get("/:id/articles", userController.getUserArticles);
router.get("/:id", userController.getUserData);
module.exports = router;
