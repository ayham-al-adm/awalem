const {
    Model,
} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Read extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
        static associate(models) {
            Read.belongsTo(models.User);
            Read.belongsTo(models.Article);
        }
    }
    Read.init({
        userId: DataTypes.BIGINT,
        articleId: DataTypes.BIGINT,
    }, {
        sequelize,
        modelName: "Read",
    });
    return Read;
};
