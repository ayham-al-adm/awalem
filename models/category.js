const {
    Model,
} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Category extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
        static associate(models) {
            Category.hasMany(models.Category, { as: "subCategories", foreignKey: "baseCategory", useJunctionTable: false });
            Category.hasMany(models.Subscribe);
            Category.hasMany(models.Article);
        }
    }
    Category.init({
        arName: DataTypes.STRING,
        enName: DataTypes.STRING,
        image: DataTypes.STRING,
        baseCategory: DataTypes.BIGINT,
    }, {
        sequelize,
        modelName: "Category",
    });
    return Category;
};
