const {
    Model,
} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Comment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
        static associate(models) {
            Comment.belongsTo(models.User);
            Comment.belongsTo(models.Article, { foreignKey: "ArticleId" });
            Comment.hasMany(models.Comment, { as: "replies", foreignKey: "baseComment" });
        }
    }
    Comment.init({
        userId: DataTypes.BIGINT,
        articleId: DataTypes.BIGINT,
        baseComment: DataTypes.BIGINT,
        text: DataTypes.STRING,
        image: DataTypes.STRING,
    }, {
        sequelize,
        modelName: "Comment",
    });
    return Comment;
};
