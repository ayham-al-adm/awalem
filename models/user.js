const {
    Model,
} = require("sequelize");
const bcrypt = require("bcrypt");
const randomString = require("randomstring");

module.exports = (sequelize, DataTypes) => {
    class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
        static associate(models) {
            User.hasMany(models.Article);
            User.hasOne(models.Reader);
            User.hasOne(models.Writer);
            User.hasMany(models.Like);
            User.hasMany(models.Comment);
            User.hasMany(models.Read);
            User.hasMany(models.Subscribe);
            User.hasMany(models.UserToken);
        }
    }
    User.init({
        name: DataTypes.STRING,
        image: DataTypes.STRING,
        email: DataTypes.STRING,
        bio: DataTypes.STRING,
        password: DataTypes.STRING,
        verificationCode: DataTypes.STRING,
        role: DataTypes.STRING,
        likesCount: DataTypes.INTEGER,
        readsCount: DataTypes.INTEGER,
        verified: DataTypes.BOOLEAN,
    }, {
        sequelize,
        modelName: "User",
    });

    User.beforeCreate((user) => {
        if (user.password) user.setDataValue("password", bcrypt.hashSync(user.getDataValue("password"), 10));
        user.setDataValue("verificationCode", randomString.generate({ charset: "0123456789", length: 6 }));
    });

    return User;
};
