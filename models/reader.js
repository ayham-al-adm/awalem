const {
    Model,
} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Reader extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
        static associate(models) {
            Reader.belongsTo(models.User);
        }
    }
    Reader.init({
        facebookId: DataTypes.STRING,
        facebookToken: DataTypes.STRING,
        googleId: DataTypes.STRING,
        googleToken: DataTypes.STRING,
        userId: DataTypes.BIGINT,
    }, {
        sequelize,
        modelName: "Reader",
    });
    return Reader;
};
