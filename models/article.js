const {
    Model,
} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Article extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
        static associate(models) {
            Article.hasMany(models.Like);
            Article.hasMany(models.Comment);
            Article.hasMany(models.Read);
            Article.hasMany(models.Section);
            Article.hasMany(models.Save, { foreignKey: "articleId" });
            Article.belongsTo(models.User);
            Article.belongsTo(models.Category);
        }
    }
    Article.init({
        title: DataTypes.STRING,
        userId: DataTypes.BIGINT,
        categoryId: DataTypes.BIGINT,
        keywords: DataTypes.TEXT,
        likesCount: DataTypes.INTEGER,
        readsCount: DataTypes.INTEGER,
        commentsCount: DataTypes.INTEGER,
    }, {
        sequelize,
        modelName: "Article",
    });
    return Article;
};
