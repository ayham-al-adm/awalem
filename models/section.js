const {
    Model,
} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Section extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
        static associate(models) {
            Section.belongsTo(models.Article);
        }
    }
    Section.init({
        articleId: DataTypes.BIGINT,
        image: DataTypes.STRING,
        title: DataTypes.STRING,
        text: DataTypes.TEXT,
        order: DataTypes.INTEGER,
    }, {
        sequelize,
        modelName: "Section",
    });
    return Section;
};
