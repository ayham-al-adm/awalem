const {
    Model,
} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Writer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
        static associate(models) {
            Writer.belongsTo(models.User);
        }
    }
    Writer.init({
        userId: DataTypes.BIGINT,
        readsCount: DataTypes.BIGINT,
        likesCount: DataTypes.BIGINT,
        articlesCount: DataTypes.BIGINT,
    }, {
        sequelize,
        modelName: "Writer",
    });
    return Writer;
};
