const {
    Model,
} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class EmailSubscribers extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
        // static associate(models) {

        // }
    }
    EmailSubscribers.init({
        email: DataTypes.STRING,
    }, {
        sequelize,
        modelName: "EmailSubscribers",
    });
    return EmailSubscribers;
};
