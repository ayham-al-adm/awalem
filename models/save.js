const {
    Model,
} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Save extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
        static associate(models) {
            Save.belongsTo(models.Article);
            Save.belongsTo(models.User);
        }
    }
    Save.init({
        userId: DataTypes.BIGINT,
        articleId: DataTypes.BIGINT,
    }, {
        sequelize,
        modelName: "Save",
    });
    return Save;
};
