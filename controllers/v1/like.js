const { Like } = require("../../models");
const { returnSuccessResponse, returnErrorResponse } = require("../../util/response");

module.exports.addLike = async (req, res) => {
    try {
        const like = await Like.create({ articleId: req.body.articleId, userId: req.user.id });
        return returnSuccessResponse(201, { data: like }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.deleteLike = async (req, res) => {
    try {
        const result = await Like.destroy({
            where: {
                userId: req.user.id,
                articleId: req.query.articleId,
            },
        });
        return returnSuccessResponse(200, { data: { deleted: result !== 0 } }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};
