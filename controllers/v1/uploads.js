const imgur = require("imgur");
const path = require("path");
const https = require("https");
const Stream = require("stream").Transform;
const fs = require("fs");

const downloadImage = (url) => {
    https.request(url, (response) => {
        const data = new Stream();

        response.on("data", (chunk) => {
            data.push(chunk);
        });

        response.on("end", () => {
            const fName = url.substring(url.lastIndexOf("/"));
            const p = path.join(__dirname, "..", "..", "public", "uploads", "images", fName);
            fs.writeFileSync(p, data.read());
        });
    }).end();
};

module.exports.uploadImage = async (req, res) => {
    // eslint-disable-next-line quotes
    try {
        if (!req.file) return res.status(500).json({ message: "file not found !" });
        const p = path.join(__dirname, "..", "..", "public", "uploads", "tmp", req.file.filename);
        console.log(p);
        const result = await imgur.uploadFile(p);
        downloadImage(result.link);
        return res.status(201).json({ path: result.link });
    } catch (error) {
        console.log(error.message, error);
        return res.status(500).json(error.message);
    }
};
