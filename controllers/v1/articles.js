const { Op } = require("sequelize");
const {
    Section, Article, sequelize, User, Writer, Comment, Like, Category, Save,
} = require("../../models");
const { returnSuccessResponse, returnErrorResponse } = require("../../util/response");

const extractArticleDataFromReq = (req) => ({
    title: req.body.title,
    categoryId: req.body.categoryId,
    userId: req.user.id,
    keywords: req.body.keywords,
});

const extractSectionsDataFromReq = (req, articleId) => {
    const sections = [];
    req.body.sections.forEach((section) => {
        sections.push({
            articleId,
            image: section.image,
            title: section.title,
            text: section.text,
            order: section.order,
        });
    });
    return sections;
};

module.exports.addArticle = async (req, res) => {
    const transaction = await sequelize.transaction();
    try {
        const articleData = extractArticleDataFromReq(req);
        const article = await Article.create(articleData, { transaction });
        const sectionsData = extractSectionsDataFromReq(req, article.dataValues.id);
        const sections = await Section.bulkCreate(sectionsData, { transaction });
        await transaction.commit();
        return returnSuccessResponse(201, { data: { article, sections } }, res);
    } catch (error) {
        await transaction.rollback();
        return returnErrorResponse(500, error.message, res);
    }
};

module.exports.getArticle = async (req, res) => {
    try {
        const article = await Article.findByPk(req.params.id, {
            include: [{
                model: User,
                attributes: ["id", "name", "image", "email", "role", "bio"],
                include: [{
                    model: Writer,
                    attributes: ["id", "readsCount", "likesCount", "articlesCount"],
                }],
            }, {
                model: Section,
            },
            ],
        });
        return returnSuccessResponse(200, { data: article }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getComments = async (req, res) => {
    try {
        const page = req.query.page ?? 1; const count = 10; const
            offset = (page - 1) * count;

        const comments = await Comment.findAll({
            subQuery: false,
            attributes: [
                "id", "userId", "image", "text", "createdAt", "articleId",
                [sequelize.fn("COUNT", sequelize.col("replies.id")), "repliesCount"],
            ],
            where: { ArticleId: req.params.id, baseComment: null },
            order: [["createdAt", "DESC"]],
            offset,
            limit: count,
            include: [{
                model: Comment,
                as: "replies",
                attributes: [],
            }, {
                model: User,
                attributes: ["id", "image", "name", "readsCount"],
            }],
            group: ["Comment.id"],
        });

        return returnSuccessResponse(200, { data: comments }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.iLike = async (req, res) => {
    try {
        const like = await Like.findOne({
            where: {
                articleId: req.params.id, userId: req.user.id,
            },
        });
        return returnSuccessResponse(200, { data: { iLike: !!like } }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.iSave = async (req, res) => {
    try {
        const save = await Save.findOne({
            where: {
                articleId: req.params.id, userId: req.user.id,
            },
        });
        return returnSuccessResponse(200, { data: { iSave: !!save } }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getTopArticles = async (req, res) => {
    try {
        const page = req.query.page ?? 1;
        const count = 20;
        const offset = (page - 1) * count;

        const articles = await Article.findAll({
            subQuery: false,
            attributes: [
                "id", "title", "createdAt", "categoryId", "readsCount", "commentsCount", "likesCount",
            ],
            include: [
                {
                    model: Section,
                    limit: 1,
                    attributes: ["id", "image", "title", "text", "order"],
                }, {
                    model: User,
                    attributes: ["id", "name", "image"],
                },
            ],
            order: [[sequelize.literal("readsCount"), "DESC"]],
            offset,
            limit: count,
        });

        return returnSuccessResponse(200, { data: articles }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getTopArticlesByCategory = async (req, res) => {
    try {
        const page = req.query.page ?? 1;
        const count = 6;
        const offset = (page - 1) * count;

        const categories = await Category.findAll({
            subQuery: false,
            include: [{
                model: Article,
                limit: 3,
                order: [["readsCount", "DESC"]],
                attributes: ["id", "title", "createdAt", "userId", "commentsCount", "readsCount", "likesCount"],
                required: true,
                include: [
                    {
                        model: User,
                        attributes: ["id", "name", "image"],
                    },
                    {
                        model: Section,
                        limit: 1,
                        attributes: ["id", "text", "title", "order", "image"],
                    },
                ],
            }],
            offset,
            limit: count,
        });

        return returnSuccessResponse(200, { data: categories }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getNewArticlesByCategory = async (req, res) => {
    try {
        const page = req.query.page ?? 1;
        const count = 6;
        const offset = (page - 1) * count;

        const categories = await Category.findAll({
            include: [{
                model: Article,
                limit: 3,
                attributes: ["id", "title", "createdAt", "userId", "commentsCount", "readsCount", "likesCount"],
                required: true,
                order: [["createdAt", "DESC"]],
                include: [
                    {
                        model: User,
                        attributes: ["id", "name", "image"],
                    },
                    {
                        model: Section,
                        limit: 1,
                        attributes: ["id", "text", "title", "order", "image"],
                    },
                ],
            }],
            offset,
            limit: count,
        });

        return returnSuccessResponse(200, { data: categories }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.search = async (req, res) => {
    try {
        const page = req.query.page ?? 1;
        const count = 20;
        const offset = (page - 1) * count;

        const articles = await Article.findAll({
            where: {
                title: {
                    [Op.like]: `%${req.query.keyword}%`,
                },
            },
            include: [
                {
                    model: User,
                    attributes: ["id", "image", "name"],
                },
                {
                    model: Section,
                    attributes: ["id", "image", "text", "title"],
                    limit: 1,
                },
            ],
            offset,
            limit: count,
        });

        return returnSuccessResponse(200, { data: articles }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

const extractWhereConditionsByOr = (req) => {
    const conditions = [];
    const keywords = req.query.keywords.split(",");
    for (let i = 0; i < keywords.length; i++) {
        conditions.push({
            keywords: {
                [Op.like]: `%${req.query.keywords[i]}%`,
            },
        });
    }
    return {
        [Op.and]: conditions,
    };
};

module.exports.searchRelative = async (req, res) => {
    try {
        const page = req.query.page ?? 1;
        const count = 10;
        const offset = (page - 1) * count;

        const whereConditions = extractWhereConditionsByOr(req);
        const articles = await Article.findAll({
            where: whereConditions,
            include: [
                {
                    model: User,
                    attributes: ["id", "image", "name"],
                },
                {
                    model: Section,
                    attributes: ["id", "image", "text", "title"],
                    limit: 1,
                },
            ],
            offset,
            limit: count,
        });

        return returnSuccessResponse(200, { data: articles }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};
