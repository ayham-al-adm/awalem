const { Op } = require("sequelize");
const {
    User, Writer, Category, Article, sequelize, Read, Section,
} = require("../../models/index");
const { returnErrorResponse, returnSuccessResponse } = require("../../util/response");

module.exports.getUserData = async (req, res) => {
    try {
        const user = await User.findByPk(req.params.id,
            {
                attributes: ["id", "bio", "name", "image", "likesCount", "readsCount", "role"]
                    .concat([
                        [sequelize.literal("(SELECT COUNT(`moreUsers`.id) + 1 FROM `USERS` AS `moreUsers` WHERE `moreUsers`.role = 'READER' AND `moreUsers`.readsCount > `User`.readsCount)"), "readsRate"],
                    ]),
                include: [{
                    model: Writer,
                    attributes: ["id", "readsCount", "likesCount", "articlesCount"],
                }],
            });
        return returnSuccessResponse(200, { data: user }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getUserArticles = async (req, res) => {
    try {
        const page = req.query.page ?? 1;
        const count = 10;
        const offset = (page - 1) * count;
        const categories = await Category.findAll({
            limit: count,
            offset,
            include: [{
                model: Article,
                where: { userId: req.params.id },
                required: true,
                include: [{
                    model: Section,
                    attributes: ["id", "image", "title", "text", "order"],
                    limit: 1,
                }, {
                    model: User,
                    attributes: ["id", "image", "name"],
                }],
            }],
        });

        return returnSuccessResponse(200, { data: categories }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getUserRate = async (req, res) => {
    try {
        const result = await User.findAll({
            attributes: [
                "id", "name", "image",
            ].concat([
                [sequelize.literal("(SELECT COUNT(`moreUsers`.id) + 1 FROM `USERS` AS `moreUsers` WHERE `moreUsers`.readsCount > `User`.readsCount)"), "rate"],
            ]),
        });
        return returnSuccessResponse(200, { data: result }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getBestReaders = async (req, res) => {
    try {
        const page = req.query.page ?? 1;
        const count = 10;
        const offset = (page - 1) * count;

        const date = new Date();
        date.setMonth(date.getMonth() - 1);
        console.log(date);
        const readers = await User.findAll({
            subQuery: false,
            attributes: ["id", "name", "image",
                [sequelize.fn("COUNT", sequelize.col("Reads.id")), "lastReadsCount"],
            ],
            include: [{
                model: Read,
                required: false,
                attributes: [],
                where: {
                    createdAt: {
                        [Op.gte]: date,
                    },
                },
            }],
            group: "Reads.id",
            order: [[sequelize.literal("lastReadsCount"), "DESC"]],
            offset,
            limit: count,
        });

        return returnSuccessResponse(200, { data: readers }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};
