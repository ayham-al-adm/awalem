const {
    Category, Article, Section, User,
} = require("../../models");
const { returnSuccessResponse, returnErrorResponse } = require("../../util/response");

const extractCategoryDataFromReq = (req) => ({
    image: req.body.image,
    enName: req.body.enName,
    arName: req.body.arName,
    baseCategory: req.body.baseCategory,
});

module.exports.addCategory = async (req, res) => {
    try {
        const category = await Category.create(extractCategoryDataFromReq(req));
        return returnSuccessResponse(201, { data: category }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getAllWithSubs = async (req, res) => {
    try {
        const categories = await Category.findAll({
            where: {
                baseCategory: null,
            },
            include: [{
                model: Category,
                as: "subCategories",
            }],
        });

        return returnSuccessResponse(200, { data: categories }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getCategoryData = async (req, res) => {
    try {
        const category = await Category.findByPk(req.params.categoryId);

        return returnSuccessResponse(200, { data: category }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getArticles = async (req, res) => {
    try {
        const page = req.query.page ?? 1;
        const count = 20;
        const offset = (page - 1) * count;

        const articles = await Article.findAll({
            subQuery: false,
            where: {
                categoryId: req.params.categoryId,
            },
            include: [
                {
                    model: User,
                    attributes: ["id", "name", "image"],
                }, {
                    model: Section,
                    attributes: ["id", "title", "text", "image"],
                    limit: 1,
                },
            ],
            order: [["createdAt", "DESC"]],
            offset,
            limit: count,
        });

        return returnSuccessResponse(200, { data: articles }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};
