const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const randomString = require("randomstring");
const axios = require("axios");
const { sequelize } = require("../../models");
const {
    User, Reader, UserToken, Subscribe,
} = require("../../models");
const mailer = require("../../util/mailer");
const userRoles = require("../../util/user.roles");
const { returnSuccessResponse, returnErrorResponse } = require("../../util/response");

const userIsExists = async (email) => {
    const user = await User.findOne({
        where: { email },
    });
    if (user === null) {
        return false;
    } return user;
};

const extractUserDataFromReq = (req) => ({
    email: req.body.email,
    password: req.body.password,
    name: req.body.name,
    image: req.body.image,
    bio: req.body.bio,
    verified: true,
});

const storeUserToken = async (userId, token) => {
    await UserToken.create({ userId, token });
};

const insertSubscribeCats = async (userId, cats) => {
    const subscribes = [];
    for (let i = 0; i < cats.length; i++) {
        subscribes.push({ userId, categoryId: cats[i] });
    }
    await Subscribe.bulkCreate(subscribes);
};

module.exports.signup = async (req, res) => {
    try {
        let user = await userIsExists(req.body.email);
        // user already exists and verified
        if (user && user.verified) {
            return res.status(403).json({ error: "Your email is already in use!" });
        }
        // user doesn't exist
        if (!user) {
            const userData = extractUserDataFromReq(req);
            user = await User.create({ ...userData, role: userRoles.reader });
            await insertSubscribeCats(user.id, req.body.categories);
        }
        return returnSuccessResponse(201, {
            user: {
                id: user.id,
                name: user.name,
                image: user.image,
                email: user.email,
            },
        }, res);
    } catch (e) {
        return returnErrorResponse(500, e, res);
    }
};

module.exports.signupWriter = async (req, res) => {
    try {
        if (await userIsExists(req.body.email)) {
            return res.status(403).json({ error: "Your email is already in use!" });
        }
        const userData = extractUserDataFromReq(req);
        const user = await User.create({ ...userData, role: userRoles.writer });
        return returnSuccessResponse(201, {
            user: {
                id: user.id,
                name: user.name,
                image: user.image,
                email: user.email,
            },
        }, res);
    } catch (e) {
        return returnErrorResponse(500, e, res);
    }
};

module.exports.verifyEmail = async (req, res) => {
    const user = await User.findOne({ where: { email: req.body.email } });
    if (user) {
        if (user.verificationCode === req.body.verificationCode) {
            user.verified = true;
            await user.save();
            return res.status(200).json({
                success: true,
            });
        }
        return res.status(403).json({ message: "Invalid email and/or verification code" });
    }
    return res.status(401).json({ message: "Invalid email and/or verification code" });
};

module.exports.login = async (req, res) => {
    try {
        const user = await userIsExists(req.body.email);
        if (!user) {
            return returnErrorResponse(401, { message: "Invalid email and/or password" }, res);
        }
        if (!bcrypt.compareSync(req.body.password, user.password)) {
            return returnErrorResponse(401, { message: "Invalid email and/or password" }, res);
        }
        if (!user.verified) {
            return returnErrorResponse(403, { message: "Please verify your email!" }, res);
        }
        const data = {
            id: user.id,
            name: user.name,
            image: user.image,
            email: user.email,
            role: user.role,
        };
        const token = jwt.sign({
            ...data,
        }, process.env.JWT_SECRET);
        await storeUserToken(user.id, token);
        return returnSuccessResponse(200, {
            user: { ...data },
            token,
        }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.resendVerificationCode = async (req, res) => {
    try {
        const user = await User.findOne({ where: { email: req.body.email } });
        if (user !== null) {
            user.verificationCode = randomString.generate({ charset: "0123456789", length: 6 });
            await user.save();
            res.status(200).json({
                success: true,
            });
            mailer.sentVerificationMail(user.email, user.verificationCode).catch(() => { });
        } else {
            res.status(401).json({ success: false });
        }
    } catch (e) {
        returnErrorResponse(500, e, res);
    }
};

module.exports.resetPassword = async (req, res) => {
    try {
        const user = await User.findOne({
            where: {
                email: req.body.email,
                verificationCode: req.body.verificationCode,
            },
        });
        if (user !== null) {
            user.password = bcrypt.hashSync(req.body.password, 10);

            await user.save();
            res.status(200).json({ success: true });
        } else {
            res.status(401).json({ message: "Invalid email and/or verification code" });
        }
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

const getFacebookUserData = async (accessToken) => {
    const data = await axios.get(`https://graph.facebook.com/me?fields=id,name,email,picture&access_token=${accessToken}`);
    if (!data.status === 200) {
        return false;
    }
    return data.data;
};

const getGoogleUserData = async (accessToken) => {
    const response = await axios.get(`https://oauth2.googleapis.com/tokeninfo?access_token=${accessToken}`);
    if (response.data.error) {
        return false;
    }
    return response.data;
};

const createNewFBUser = async (data, fbToken, fbId) => {
    const result = await sequelize.transaction(async (transaction) => {
        const user = await User.create({
            email: data.email,
            name: data.name,
            image: data.picture.data.url,
            role: userRoles.reader,
        }, { transaction });
        await Reader.create({
            userId: user.dataValues.id, facebookToken: fbToken, facebookId: fbId,
        }, { transaction });
        return user;
    });

    return result.dataValues;
};

const createNewGoogleUser = async (data, googleToken, googleId) => {
    const result = await sequelize.transaction(async (transaction) => {
        const user = await User.create({
            email: data.email,
            name: `${data.givenName} ${data.familyName}`,
            image: data.imageUrl,
            role: userRoles.reader,
        }, { transaction });
        await Reader.create({
            userId: user.dataValues.id, googleToken, googleId,
        }, { transaction });
        return user;
    });

    return result.dataValues;
};

const generateLoginDataForFBUser = async (user) => {
    const token = jwt.sign({ ...user }, process.env.JWT_SECRET);
    await UserToken.create({
        token,
        userId: user.id,
    });
    return { user, token };
};

const generateLoginDataForGoogleUser = async (user) => {
    const token = jwt.sign({ ...user }, process.env.JWT_SECRET);
    await UserToken.create({
        token,
        userId: user.id,
    });
    return { user, token };
};

const fbUserIsExists = async (facebookId) => {
    const user = await Reader.findOne({
        where: { facebookId },
        include: [{ model: User, atributes: ["id", "name", "image", "role"] }],
    });
    if (user === null) {
        return false;
    } return user.User;
};

const googleUserIsExists = async (googleId) => {
    const user = await Reader.findOne({
        where: { googleId },
        include: [{ model: User, atributes: ["id", "name", "image", "role"] }],
    });
    if (user === null) {
        return false;
    } return user.User;
};

module.exports.loginByFacebook = async (req, res) => {
    try {
        const userFBData = await getFacebookUserData(req.body.facebookToken);
        if (!userFBData) {
            return returnErrorResponse(500, { message: "Failed to verify facebook account!" }, res);
        }
        let userDBData = await fbUserIsExists(userFBData.id);
        if (!userDBData) {
            userDBData = await createNewFBUser(userFBData,
                req.body.facebookToken, req.body.facebookId);
        }
        const userLoginData = await generateLoginDataForFBUser(userDBData);
        return returnSuccessResponse(200, userLoginData, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.loginByGoogle = async (req, res) => {
    try {
        const isGoogleUser = await getGoogleUserData(req.body.googleToken);
        if (!isGoogleUser) {
            return returnErrorResponse(401, { message: "Failed to verify google account!" }, res);
        }
        let userDBData = await googleUserIsExists(req.body.profile.googleId);
        if (!userDBData) {
            userDBData = await createNewGoogleUser(req.body.profile,
                req.body.googleToken, req.body.googleId);
        }

        const userLoginData = await generateLoginDataForGoogleUser(userDBData);

        return returnSuccessResponse(200, userLoginData, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.refreshToken = async (req, res) => {
    try {
        // find token if is exists
        const token = await UserToken.findOne({
            where: {
                token: req.body.token,
            },
            include: {
                model: User,
                attributes: ["id", "name", "image", "email", "role"],
            },
        });

        if (!token) {
            return returnErrorResponse(401, { message: "Failed to verify token" }, res);
        }
        // generate new token, delete old one and save new one
        const newToken = jwt.sign({
            ...token.User.dataValues,
        }, process.env.JWT_SECRET);

        // delete and save transaction
        await sequelize.transaction(async (transaction) => {
            await UserToken.destroy({
                where: { token: req.body.token },
            }, { transaction });
            await UserToken.create({
                token: newToken,
                userId: token.User.id,
            }, { transaction });
        });
        return returnSuccessResponse(200, { token: newToken }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};
