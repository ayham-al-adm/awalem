const { Subscribe } = require("../../models");
const { returnSuccessResponse, returnErrorResponse } = require("../../util/response");

module.exports.addSubscribe = async (req, res) => {
    try {
        const subscribe = await Subscribe.create({
            categoryId: req.body.categoryId, userId: req.user.id,
        });
        return returnSuccessResponse(201, { data: subscribe }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.deleteSubscribe = async (req, res) => {
    try {
        const result = await Subscribe.destroy({
            where: {
                userId: req.user.id,
                categoryId: req.query.categoryId,
            },
        });
        return returnSuccessResponse(200, { data: { deleted: result !== 0 } }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};
