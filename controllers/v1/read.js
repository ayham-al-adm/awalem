const { Sequelize } = require("sequelize");
const {
    Read, Article, User, Section, Category,
} = require("../../models");
const { returnSuccessResponse, returnErrorResponse } = require("../../util/response");

module.exports.addRead = async (req, res) => {
    try {
        const read = await Read.create({ articleId: req.body.articleId, userId: req.user.id });
        return returnSuccessResponse(201, { data: read }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getReads = async (req, res) => {
    try {
        const page = req.query.page ?? 1;
        const count = 10;
        const offset = (page - 1) * count;

        const reads = await Read.findAll({
            subQuery: false,
            where: {
                userId: req.user.id,
            },
            order: [["createdAt", "DESC"]],
            offset,
            limit: count,
            include: [{
                model: Article,
                include: [{
                    model: Section,
                    attributes: ["id", "image", "title", "text", "order"],
                }, {
                    model: User,
                    attributes: ["id", "image", "name"],
                }],
            }],
        });

        return returnSuccessResponse(200, { data: reads }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getRecentlyReadArticlesByCategory = async (req, res) => {
    try {
        const page = req.query.page ?? 1;
        const count = 6;
        const offset = (page - 1) * count;

        const categories = await Category.findAll({
            subQuery: false,
            limit: count,
            offset,
            include:
            {
                model: Article,
                required: true,
                where: Sequelize.literal(`Articles.id IN (SELECT articleId FROM \`Reads\` WHERE userId = ${req.user.id})`),
                include: [{
                    model: User,
                    attributes: ["id", "name", "image", "role"],
                }, {
                    model: Section,
                },
                {
                    model: Read,
                    attributes: ["id"],
                    where: Sequelize.literal(`\`Articles->Reads\`.userId = ${req.user.id}`),
                }],
            },
        });

        return returnSuccessResponse(200, { data: categories }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};
