const {
    User, Article, Section, Comment, Category,
} = require("../../models/index");
const { returnSuccessResponse, returnErrorResponse } = require("../../util/response");

module.exports.getUsers = async (req, res) => {
    try {
        const page = req.query.page ?? 1;
        const count = 20;
        const offset = (page - 1) * count;
        const users = await User.findAll({
            order: [["createdAt", "DESC"]],
            offset,
            limit: count,
        });
        return returnSuccessResponse(200, users, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.editUser = async (req, res) => {
    try {
        const user = await User.findOne({
            where: { id: req.params.id },
        });

        user.name = req.body.name;
        user.image = req.body.image;
        user.email = req.body.email;
        user.bio = req.body.bio;
        user.verificationCode = req.body.verificationCode;
        user.role = req.body.role;
        user.verified = req.body.verified;

        await user.save();

        return returnSuccessResponse(200, user, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.deleteUser = async (req, res) => {
    try {
        await User.destroy({
            where: {
                id: req.params.id,
            },
        });

        return returnSuccessResponse(200, { data: { message: "Deleted Success!" } }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.addUser = async (req, res) => {
    try {
        const user = new User();
        user.bio = req.body.bio;
        user.name = req.body.name;
        user.image = req.body.image;
        user.email = req.body.email;
        user.password = req.body.password;
        user.role = req.body.role;
        await user.save();

        return returnSuccessResponse(200, user, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getArticles = async (req, res) => {
    try {
        const page = req.query.page ?? 1;
        const count = 20;
        const offset = (page - 1) * count;
        const articles = await Article.findAll({
            order: [["createdAt", "DESC"]],
            offset,
            limit: count,
        });
        return returnSuccessResponse(200, articles, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.editArticle = async (req, res) => {
    try {
        const article = await Article.findOne({
            where: { id: req.params.id },
        });

        article.title = req.body.title;
        article.userId = req.body.userId;
        article.categoryId = req.body.categoryId;
        article.keywords = req.body.keywords;
        await article.save();

        return returnSuccessResponse(200, article, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.deleteArticle = async (req, res) => {
    try {
        console.log(req.params.id);
        await Article.destroy({
            where: {
                id: req.params.id,
            },
        });

        return returnSuccessResponse(200, { data: { message: "Deleted Success!" } }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.addArticle = async (req, res) => {
    try {
        const article = new Article();
        article.title = req.body.title;
        article.userId = req.body.userId;
        article.categoryId = req.body.categoryId;
        article.keywords = req.body.keywords;
        await article.save();

        return returnSuccessResponse(200, article, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getSections = async (req, res) => {
    try {
        const page = req.query.page ?? 1;
        const count = 20;
        const offset = (page - 1) * count;
        const sections = await Section.findAll({
            order: [["createdAt", "DESC"]],
            offset,
            limit: count,
        });
        return returnSuccessResponse(200, sections, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.editSection = async (req, res) => {
    try {
        const section = await Section.findOne({
            where: { id: req.params.id },
        });

        section.image = req.body.image;
        section.title = req.body.title;
        section.text = req.body.text;
        section.order = req.body.order;
        section.articleId = req.body.articleId;

        await section.save();

        return returnSuccessResponse(200, section, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.deleteSection = async (req, res) => {
    try {
        await Section.destroy({
            where: {
                id: req.params.id,
            },
        });

        return returnSuccessResponse(200, { data: { message: "Deleted Success!" } }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.addSection = async (req, res) => {
    try {
        const section = new Section();
        section.articleId = req.body.articleId;
        section.image = req.body.image;
        section.title = req.body.title;
        section.text = req.body.text;
        section.order = req.body.order;
        await section.save();

        return returnSuccessResponse(200, section, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getComments = async (req, res) => {
    try {
        const page = req.query.page ?? 1;
        const count = 20;
        const offset = (page - 1) * count;
        const comments = await Comment.findAll({
            order: [["createdAt", "DESC"]],
            offset,
            limit: count,
        });
        return returnSuccessResponse(200, comments, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.editComment = async (req, res) => {
    try {
        const comment = await Comment.findOne({
            where: { id: req.params.id },
        });

        comment.userId = req.body.userId;
        comment.articleId = req.body.articleId;
        comment.baseComment = req.body.baseComment;
        comment.text = req.body.text;
        comment.image = req.body.image;

        await comment.save();

        return returnSuccessResponse(200, comment, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.deleteComment = async (req, res) => {
    try {
        await Comment.destroy({
            where: {
                id: req.params.id,
            },
        });

        return returnSuccessResponse(200, { data: { message: "Deleted Success!" } }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.addComment = async (req, res) => {
    try {
        const comment = new Comment();
        comment.articleId = req.body.articleId;
        comment.userId = req.body.userId;
        comment.baseComment = req.body.baseComment;
        comment.text = req.body.text;
        comment.image = req.body.image;
        await comment.save();

        return returnSuccessResponse(200, comment, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getCategories = async (req, res) => {
    try {
        const page = req.query.page ?? 1;
        const count = 20;
        const offset = (page - 1) * count;
        const categories = await Category.findAll({
            order: [["createdAt", "DESC"]],
            offset,
            limit: count,
        });
        return returnSuccessResponse(200, categories, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.editCategory = async (req, res) => {
    try {
        const category = await Category.findOne({
            where: { id: req.params.id },
        });

        category.arName = req.body.arName;
        category.enName = req.body.enName;
        category.image = req.body.image;
        category.baseCategory = req.body.baseCategory;

        await category.save();

        return returnSuccessResponse(200, category, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.deleteCategory = async (req, res) => {
    try {
        await Category.destroy({
            where: {
                id: req.params.id,
            },
        });

        return returnSuccessResponse(200, { data: { message: "Deleted Success!" } }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.addCategory = async (req, res) => {
    try {
        const category = new Category();
        category.arName = req.body.arName;
        category.enName = req.body.enName;
        category.image = req.body.image;
        category.baseCateogry = req.body.baseCateogry;
        await category.save();

        return returnSuccessResponse(200, category, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};
