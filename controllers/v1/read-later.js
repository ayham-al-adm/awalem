const { Sequelize } = require("sequelize");
const {
    ReadLater, Article, Section, Category, User,
} = require("../../models");
const { returnSuccessResponse, returnErrorResponse } = require("../../util/response");

module.exports.addReadLater = async (req, res) => {
    try {
        console.log(req.body.articleId);
        const readLater = await ReadLater.create({
            userId: req.user.id,
            articleId: req.body.articleId,
        });
        return returnSuccessResponse(201, { data: readLater }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.deleteReadLater = async (req, res) => {
    try {
        const result = await ReadLater.destroy({
            where: {
                userId: req.user.id,
                articleId: req.query.articleId,
            },
        });
        return returnSuccessResponse(200, { data: { deleted: result !== 0 } }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getReadLaters = async (req, res) => {
    try {
        const count = 10;
        const page = req.query.page ?? 1;
        const offset = (page - 1) * count;

        const readLaters = await ReadLater.findAll({
            subQuery: false,
            where: { userId: req.user.id },
            attributes: ["id", "userId", "articleId", "createdAt"],
            offset,
            limit: count,
            order: [["createdAt", "DESC"]],
            include: [{
                model: Article,
                include: [{
                    attributes: ["id", "image", "title", "text", "order"],
                    model: Section,
                    limit: 1,
                }],
            }],
        });
        return returnSuccessResponse(200, { data: readLaters }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getReadlaterArticlesByCategory = async (req, res) => {
    try {
        const page = req.query.page ?? 1;
        const count = 6;
        const offset = (page - 1) * count;

        const categories = await Category.findAll({
            subQuery: false,
            limit: count,
            offset,
            include:
            {
                model: Article,
                required: true,
                where: Sequelize.literal(`Articles.id IN (SELECT articleId FROM Readlaters WHERE userId = ${req.user.id})`),
                include: [{
                    model: User,
                    attributes: ["id", "name", "image", "role"],
                }, {
                    model: Section,
                }],
            },
        });

        return returnSuccessResponse(200, { data: categories }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};
