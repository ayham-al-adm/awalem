const { Comment, User } = require("../../models");
const { returnSuccessResponse, returnErrorResponse } = require("../../util/response");

const extractCommentDataFromReq = (req) => ({
    userId: req.user.id,
    articleId: req.body.articleId,
    text: req.body.text,
    image: req.body.image,
    baseComment: req.body.baseComment,
});

module.exports.addComment = async (req, res) => {
    try {
        const comment = await Comment.create(extractCommentDataFromReq(req));
        return returnSuccessResponse(201, { data: comment }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.deleteComment = async (req, res) => {
    try {
        const comment = await Comment.findByPk(req.query.commentId);
        if (!comment) {
            return returnErrorResponse(500, { message: "This comment in not found!" }, res);
        }
        if (comment.userId !== req.user.id) {
            return returnErrorResponse(403, { message: "You have no permission to delete this comment!" }, res);
        }
        await comment.destroy();
        return returnSuccessResponse(200, { message: "Success!" }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};

module.exports.getReplies = async (req, res) => {
    try {
        const page = req.query.page ?? 1;
        const count = 10;
        const offset = (page - 1) * count;

        const replies = await Comment.findAll({
            where: {
                baseComment: req.params.id,
            },
            include: [{
                model: User,
                attributes: ["id", "name", "image"],
            }],
            order: [["createdAt", "DESC"]],
            offset,
            limit: count,
        });
        return returnSuccessResponse(200, { data: replies }, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};
