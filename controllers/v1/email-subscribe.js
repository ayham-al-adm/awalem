const {
    EmailSubscribers,
} = require("../../models/index");
const { returnSuccessResponse, returnErrorResponse } = require("../../util/response");

module.exports.add = async (req, res) => {
    try {
        let sub = await EmailSubscribers.findOne({
            where: {
                email: req.body.email,
            },
        });
        if (sub) {
            return returnSuccessResponse(200, sub, res);
        }
        sub = await EmailSubscribers.create({
            email: req.body.email,
        });
        return returnSuccessResponse(200, sub, res);
    } catch (error) {
        return returnErrorResponse(500, error, res);
    }
};
