const express = require("express");
const app = express();
const path = require("path");
const cors = require("cors");
const v1Routes = require("./routes/v1.routes");

const allowCrossDomain = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type");

    next();
};

app.use(cors());

app.use(allowCrossDomain);

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use("/api/v1", v1Routes);
app.use(express.static(path.join(__dirname, "public")));
module.exports = app;
